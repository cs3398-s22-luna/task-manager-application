# Task Mananger Desktop Application
> Emily Tacker, Terry Tosh, Julian Vu, Katia Gutierrez and Bigyan Bhandari are 
creating a desktop application where users can keep lists that track the tasks they need to accomplish. Targeted users are
students, people with busy work/personal schedules and people who struggle with time management. We are
creating this app in order to have an increased level of organization, and to learn valuable app developing skills.
><!-- Live demo [_here_](https://www.example.com).  If you have the project hosted somewhere, include the link here. -->


![](Images/TaskManagerIcon.png)

---
## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)
<!-- * [License](#license) -->

---
## General Information
<!-- - Provide general information about your project here.
- What problem does it (intend to) solve?
- What is the purpose of your project?
- Why did you undertake it?
You don't have to answer all the questions - just the ones relevant to your project. 
[####DELETE QUESTIONS LATER####] -->
 We’re creating a to-do task management app that can schedule all the tasks/events due in a to-do format,rank their priorities and set the reminder/deadline for each event.
 As college students in junior or senior years, we have several homeworkds/deadlines that are hard to manage sometimes. We'd like make our lifeeasier with this application.
 This application is helpful for everyone, especially people with busy lifestyles, as they have to manage their task/to-do thing everyday.

---
## Technologies Used
* OpenJDK11 or later version
* Netbeans 13
* Java - version 16.0.1
* BitBucket
* Jira
* Circle CI

---
## Features
* User Story: I, Emily, as a college student would like an app where I can keep track of what I need to do. <br />
    * Task Scheduler - Ability to schedule tasks on a given day <br />  
* User Story: I, Bigyan, a working professional, want a to-do app where I can input tasks, deadlines, and their priority so that I do not leave important tasks unfinished. <br />
    * Task Editor - A board that user can remove/add/check tasks <br />
* User Story: I, Terry Tosh, as a software developer, want some sort of prioty status ranking system so that user's can order their to-do list events by priority. <br />
    * Task Priority Ranker - Rank task's priority of the day <br /> 
* User Story: I, Katia Gutierrez, , as a student and full-time worker, would like a to-do list app that allows for categorization so that tasks that deal with school or work or lifestyle can remain in separate, organized, groups. <br />
    * Task Categorizer - Ability to sort/filter task based on expected completion time <br />
* User Story: I, Julian Vu, as a student, would like to a to-do app that can display a calender and task associate with each days:
    * Calender: Ability to display task in a calender <br />

---
## Screenshots
<!--[Example screenshot](./img/screenshot.png)
If you have screenshots you'd like to share, include them here.-->

---
## Setup
<!-- What are the project requirements/dependencies? Where are they listed? A requirements.txt or a Pipfile.lock file perhaps? Where is it located? 
Proceed to describe how to install / setup one's local environment / get started with the project. -->

#### Since this one is a simple offline Desktop application, requirements are not stricted as it only requires:
* A PC/Laptop that has Windows/MacOs/Linux operating system. <br />
* 7zip installed, or equivalent file-achiever <br />
#### Installation:
* Download an uncompressed zip package from our source (virus free). <br />
* Unzip to the directory of your choice. <br />
* Click in ToDo.exe to run application. <br />

---
## Usage
How does one go about using it?
Provide various use cases and code examples here.

`write-your-code-here`

---
## Project Status
<!-- Project is: _in progress_ / _complete_ / _no longer being worked on_. If you are no longer working on it, provide reasons why.
<br /> -->
Project Status: in progress

---
## Sprint 1 Review

### Terry Tosh

#### URL References: <br />
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/src/master/NetBeans%20Research/tmt103%20-%20NB%20Research.txt <br />
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/src/master/Task%20Manager%20Application/taskmanager/TaskManagerMainGUI.java <br />
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/src/master/Task%20Manager%20Application/taskmanager/AddTaskGUI.java
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/e229ace27473101d1369e3057c6a25f915868f41 <br />
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/47c72681f64c6442aca211cbf7e1a6082b3544c0 <br />

#### Status: <br />
* Researched basic information for downloading NetBeans and creating new projects with GUIs. This was used by my team to create the initial codebase <br />
* Designed and implemented main application GUI <br />
* Designed and implemented add task GUI <br />
* Polished folder system for BitBucket repository <br />

![](Images/AddTaskGUI.JPG)
![](Images/MainApplicationGUI.JPG)

#### Next steps:
* Polish main application and add task GUI to improve user experience <br />
* Design more prominent container/panel for holding and displaying current tasks <br />
* Develope a way for a user to store their current tasks locally <br />
* Since local storage will be used, must implement retrieving the locally stored data <br />
       
### Emily Tacker

#### Sprint 1:
* Researched information on how to add a database to our application and created a local database. This was going to be used to hold all of the tasks the user had pending. We decided midway through the sprint to go in a different direction. Link: https://bitbucket.org/cs3398-s22-luna/task-manager-application/src/master/NetBeans%20Research/Databse%20resources.docx
* Created task famework that is used to hold the information of a singular task. Link: https://bitbucket.org/cs3398-s22-luna/task-manager-application/src/master/Task-Manager-Application/taskmanager/Task.java
* Created a task manager that contains a list of tasks. It originally was just a vector but then our team decided to change it to a default list model to better support the GUI. Link: https://bitbucket.org/cs3398-s22-luna/task-manager-application/src/master/Task-Manager-Application/taskmanager/TaskManager.java

#### Goals for Next Sprint:
* Add JUnit Testing
* Add Input Validation
* Improve Task Framework

#### Room for improvement: 
* Configuring UI design. <br />
* The frame that used for add/edit/check/remove task can be polished. <br />


### Julian Vu

#### Sprint 1:

* Implement the listener to integrate Task/TaskManager component to GUI frame. We're able to retrieve information from user input and display on the GUI panel.
* Write code to intergrate Back-End and Front-End. We create a new Task object every time a task is added. 
* Scretch a class design for Back-End use. Back-End class will have Task, TaskList, Util, Overide, TaskOperations to serve the back-end use.
* Research netbeans through documents from https://netbeans.apache.org//

#### URL Reference
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/src/master/NetBeans%20Research/tpv4-JPanel-JButton-Handle
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/332fa26b570327cea76098a02be83155095c20b0
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/src/master/Task%20Manager%20Application/taskmanager/TaskManagerMainGUI.java <br />
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/src/master/Task%20Manager%20Application/taskmanager/AddTaskGUI.java

#### Goals for Next Sprint:
 * Add JUnit Testing
 * Add Input Validation
 * Improve Task Framework
 * Applying SOLID principle for Task Manager, the Task Manager class itself will only serve purpose of intialize constructor. We'll create TaskOperations classes for the pupose of sorting or catergorize tasks.


#### Room for Improvement
* UI could be better, the layout of each textField need to be more organized.
* Needs a seperate class that handle the listener instead of having variables handle within the GUI.


#### To do: 
* Migrating tasks from each Java Swing Panels to a main desktop frame <br />  DONE
* Task priority ranking modification will be added. <br />  
* Adding every features frame into one main application frame <br />
* Developing a good class design that apply SOLID principles and design patterns to make the code reusable, make classes loosely coupling, 
easy to extend and test.


### Bigyan Bhandari

#### Sprint 1:

*	Implemented the ability to add due date, change due date, and adding and changing the priority of a task.
*	Researched the use of GUI in Netbeans through this video: https://www.youtube.com/watch?v=GZ9MT2myBf8&t=2590s
*	Studied the method to implement custom sort/ comparator method in Java and implemented a class to sort the tasks in the task list according to priority and due date.

#### URL References: <br />

* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/7b88d9bd66e8530356a46f9cf4c9a2cc70664179
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/e9275dcdaa7434f2b0b39a6dd53fad4077cab5f9

#### Goals for Next Sprint:
 
*	Add JUnit Testing 
*	Implement the feedback about having a button/method to sort and order task in the GUI.
*	Rewrite some of the abilities of the Task class to adhere to SOLID principles.
*	Help in suggesting/adding necessary features to the GUI.

## Room for Improvement

*	The UI is a work in progress, we will look to implement the feedback received in the class.
*	We could organize the code better to follow SOLID principles as much as we can.



#### To do: 

*	Adding a feature that allows to sort/order tasks displayed in the GUI based on a user’s preference.
*	Writing custom test cases for various methods.
*	Structuring the type of input and adding methods to validate the input. Date needs to be in the future, there will be a limit to the length of the text fields for task name, description – we will add methods to add these features/functions.


### Katia Gutierrez

#### Sprint 1

*   Implented the ability to mark a certain task as "Complete."
*   Did extensive research on Netbeans GUI as well as Java Swing. This was in order to become acquainted with the IDE and be able to assist in GUI work.

### URL References

*   https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/62a0b81a43bcee9a0c8e48a8af3cf14279a0b86d
*   https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/d2276fc49613d04a2ebdcae539d9359004e218e2

### Goals for Next Sprint:

*   Implement JUnit Testing.
*   Assist in polishing GUI for improved user experience.
*   Implement ability to edit tasks.

## Room for Improvement

*   Improvments in UI could be made.
*   SOLID principles need to be implemented within our code.

#### To Do:

*   Write code that allows for modification/editing of previously inputted tasks.
*   Write custom test cases.
*   Research JTable and DefaultTable models.

---
## Sprint 2 Review

### Terry Tosh

#### URL References: <br />
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/a9e1b06c067a4b0b39be19ed85d25a587a3c2228 <br />
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/1ec1d1b639cc349e9e7b30bf984180826a78a6d0 <br />
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/2a3d407e36d1b4c7a0ea86e0487a8ab63d677e36 <br />
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/1cb3d17a03b8a24cbe217ca379097689b6e3c351 <br />

#### Status: <br />
* Researched workaround for errors when importing NetBeans project to local machine <br />
* Fixed errors of images not showing up in project GUI <br />
* Fixed errors for when adding new task to list of tasks <br />

#### Next steps:
* Implement a delete task GUI <br />
* Enhance exisiting add task GUI <br />
* Begin researching local storage implementations <br />

### Emily Tacker

#### Sprint 2:
* Added a way to sort tasks by Date and Priority. Link: https://bitbucket.org/cs3398-s22-luna/task-manager-application/src/de10703242c485745d7a65afad72cf427a19569b/OLD%20GUI%20CODE/taskmanager/TaskManager.java
* Added input validation for adding a task Link: https://bitbucket.org/cs3398-s22-luna/task-manager-application/src/de10703242c485745d7a65afad72cf427a19569b/OLD%20GUI%20CODE/taskmanager/TaskManager.java
* Wrote code to update a task after its been added. Link: https://bitbucket.org/cs3398-s22-luna/task-manager-application/src/de10703242c485745d7a65afad72cf427a19569b/OLD%20GUI%20CODE/taskmanager/TaskManager.java

#### Goals for Next Sprint:
* Add more JUnit Testing
* Improve GUI functionality
* Homogenize code

#### Room for improvement: 
* Configuring UI design. <br />
* Code need to be more consistent in how it uses task variables. <br />

### Julian Vu

#### Sprint 2:
* Added Testing unit for deadline date validation <br />
* Merge the old design GUI's panel to the new designs' GUI <br />
* Refactoring the GUI code to have it more readable <br />

#### URL References: 
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/e11879b0bfc61feb407f78a13e44ad3ac7f60c84
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/bb46aa1d9cf22d674f56e0aec122df4209f1e301

#### Goals for Next Sprint:
* Integrate user's input with testing files <br />
* Refactoring MainGUI once every GUI components are finalized. <br />

#### Room for improvement: 
* Refactoring code still need to be cleaner, especially in GUI modification. <br />
* Needs to interacts more with the back-end code. <br />


### Bigyan Bhandari

#### Sprint 2:

*	Created test cases for TaskManager class.
*	Created test cases for Task class.
*	Researched workaround to an issue where Netbeans was not recognizing the java project.

#### URL References: <br />

* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/a4d6e054167e043884ee462c9aca7cb2c71ccb6d
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/3a1152daef5765c4d4a89cd6fc090013ef4a7edf

#### Goals for Next Sprint:
 
*	Integrate test cases for user input validation.
*	Work on GUI design and help refactor help rewrite of the longer methods following SOLID principles.

## Room for Improvement

*	Validating edge cases for user input could be done to ensure there are no abnormalities in the application.
*	Some of our methods are longer and we need to fix longer methods into different methods or classes.

#### To do: 

*	Test cases to cover all edge cases.
*	Connecting backend code with the frontend application.

### Katia Gutierrez

#### Status: <br />
* Researched and implemented improved way to display tasks
* Polished/re-designed main GUI
* Designed and added "Sort by" button onto main GUI

#### URL References: <br />
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/d274b82522be7aca5fd1d7fd55871b80cb4a014d
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/70eb53b3fe9e1b75bc3b9e1f13988480f17321c9
* https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/b681cad007cbf8b204b8a6b45490f99c68b8d03f

#### Goals for Next Sprint: <br />
* Create and implement edit task GUI
* Configure "Sort by" button to work with backend code
* Redesign logo

#### Room for Improvement: <br />
* Improve logo aesthetic/design, and UI in general
* Clean up/organize repository
* Improve Netbeans compatability with Gitkraken

---

## Sprint 3 Review

### Emily Tacker

#### Sprint 3:
* https://cs3398s22luna.atlassian.net/jira/software/projects/CLP/boards/1?assignee=61eb235a7ae0dc006acb81fe&selectedIssue=CLP-59 Add Junit Testing for Task Class https://bitbucket.org/cs3398-s22-luna/task-manager-application/branch/CLP-59-add-junit-testing-for-task-class
* https://cs3398s22luna.atlassian.net/jira/software/projects/CLP/boards/1?assignee=61eb235a7ae0dc006acb81fe&selectedIssue=CLP-57 Make sure dates are String and handled appropriately https://bitbucket.org/cs3398-s22-luna/%7B1d74ea18-e5eb-43f6-8b98-ce9350d47b0f%7D/branch/feature/CLP-57
* https://cs3398s22luna.atlassian.net/jira/software/projects/CLP/boards/1?assignee=61eb235a7ae0dc006acb81fe&selectedIssue=CLP-64 Add input validation into Task Class https://bitbucket.org/cs3398-s22-luna/%7B1d74ea18-e5eb-43f6-8b98-ce9350d47b0f%7D/commits/ba5282f1259b892d9f88edf87bcc7f38e625db0c

#### Goals for Next Sprint:
* Add information screen for user
* Add completed section
* Add description for Tasks


### Bigyan Bhandari

#### Sprint 3:

*	Implemented object permanence feature to store task lists using java serializable interface.
*	Reworked on GUI to add Delete button and enable task edit and delete.

#### URL References: </br>

* Task 1 : Implemented object permanence feature to store task lists using java serializable interface. </br>
  [Jira Link](https://cs3398s22luna.atlassian.net/jira/software/projects/CLP/boards/1?selectedIssue=CLP-69) </br>
  [Bitbucket Commit](https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/8a863c2ba4f14017b60fe807d21d2b8aba2b8a28) </br>
* Task 2 : Reworked on GUI to add Delete button and enable task edit and delete. </br>
  [Jira Link](https://cs3398s22luna.atlassian.net/jira/software/projects/CLP/boards/1?selectedIssue=CLP-70) </br>
  [Bitbucket Commit for Edit Task](https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/d5d034178a1159b51ec4868d4e6308eeccbf4fdf) </br>
  [Bitbucket Commit for Delete Task](https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/d5d034178a1159b51ec4868d4e6308eeccbf4fdf) </br>



#### Goals for Next Sprint/ next direction for the project:
 
*	Add a feature to display completed tasks in a separate GUI frame.
*	Add a confirmation popup before a task is deleted permanently by the user.
*   Add a calendar pop-up when a user needs to input or edit the due date of a task.

---

### Terry Tosh

#### URL References/Status: <br />
* Jira Task CLP-50: Fixed ongoing errors in MainGUI and AddGUI files - https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/272813563eed91a2f11f4fe4e62520427ba7e655 <br />
    * https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/cd996ea542bb0708508854f748baf3e97eaeec11 <br />
    * This task was necessary in order for our application to compile and run on all systems. <br />
* Jira Task CLP-74: Add list of current tasks to DeleteGUI dropdown menu - https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/8af7cc91fa852794215a4315c2d10cc35154dcac <br />
    * This feature was used to easily delete tasks in a manageable way. It allowed users to choose which task to delete from the current lists of tasks and delete it. <br />
    * Was not implemented in the demo branch due to merge conflicts, but is present in main branch. <br />
* Jira Task CLP-67: Create popup GUI for deleting a task from current list - https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/b42230aeb3ac9a3de8dc2c42ba1a5fc2b175c307 <br />
    * This feature added the popup GUI that appeared when a user clicks the delete task button. Allowed for a smoother user experience. <br />
    * Was not implemented in the demo branch due to merge conflicts, but is present in main branch. <br />
* Jira Task CLP-66: Add button for deleting task - https://bitbucket.org/cs3398-s22-luna/task-manager-application/commits/d67489844fa5af38e270aedfb9d479c1882ce32c <br />
    * This implementation was necessary in order to allow a user to delete a task. Once clicked, the button triggered the delete task popup GUI. <br />
* Jira Task CLP-68: Add scrollbar functionality to the panel containing current list of tasks - https://bitbucket.org/cs3398-s22-luna/task-manager-application/branch/feature/CLP-68-add-scrollbar-functionality-to-ta <br />
    * Feature was added to the main task panel containing all the current tasks. Allowed the user to be able to scroll through tasks once they filled the entire panel. <br />

![](Images/DeleteGUI.JPG)
![](Images/DeleteDropdown.png)

#### Next steps:
* Display the priority of a task as "high", "medium", or "low" string instead of an integer so that it is more clear to the user. <br />
* Create user accounts in order to sign in and view application from various systems. <br />
* Tweak appearance of all GUIs for a more modern application look and feel. <br />



### Julian Vu

#### Sprint 3:

*	Write code to throw error message if user input wrong message
*	Fix some bug where task is automatically deleted if date edited is invalid
*   Refactoring MainGUI code to make code generally readable.

#### URL References: </br>

* Task 1 : Write code to throw error message if user input wrong message </br>
  [Jira Link](https://cs3398s22luna.atlassian.net/jira/software/projects/CLP/boards/1?selectedIssue=CLP-55) </br>
  [Bitbucket Commit](https://bitbucket.org/cs3398-s22-luna/task-manager-application/pull-requests/14/feature-clp-71-added-some-pop-up-error) </br>
* Task 2 : Fix some bug where task is automatically deleted if date edited is invalid</br>
  [Jira Link](https://cs3398s22luna.atlassian.net/jira/software/projects/CLP/boards/1?selectedIssue=CLP-73) </br>
  [Bitbucket Commit](https://bitbucket.org/cs3398-s22-luna/%7B1d74ea18-e5eb-43f6-8b98-ce9350d47b0f%7D/pull-requests/19)</br>
* Task 3 : Refactoring MainGUI code to make code generally readable. </br>
  [Jira Link](https://cs3398s22luna.atlassian.net/jira/software/projects/CLP/boards/1?selectedIssue=CLP-58)</br>
  [Bitbucket Commit](https://bitbucket.org/cs3398-s22-luna/%7B1d74ea18-e5eb-43f6-8b98-ce9350d47b0f%7D/pull-requests/18) </br>



#### Goals for Next Sprint/ next direction for the project:
 
*   Add a calendar pop-up and organize functions
*   Has installer and configuration for user ready to use.
*   Automated task function to other calendar platform such as Google Calendar.

### Katia Gutierrez

#### Sprint 3

*  Cleaned up main GUI.

#### URL References: </br>
*  https://bitbucket.org/cs3398-s22-luna/task-manager-application/branch/CLP-61-clean-up-gui

#### Goals for Next Sprint

*  Calender pop-up
*  Improve logo and aestheics of main UI.
*  Implement for JUnit tests.

---
## Contact
Emily Tacker: eat111@txstate.edu

Terry Tosh: tmt103@txtstate.edu

Julian Vu: tpv4@txstate.edu

Katia Gutierrez: kmg320@txstate.edu

Bigyan Bhandari: bbb95@txstate.edu

<!-- Optional -->
<!-- ## License -->
<!-- This project is open source and available under the [... License](). -->

<!-- You don't have to include all sections - just the one's relevant to your project -->