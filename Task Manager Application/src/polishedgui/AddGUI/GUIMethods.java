package polishedgui.AddGUI;

import javax.swing.*;

import polishedgui.Task;
import polishedgui.TaskManager;
import polishedgui.MainGUI.MainGUI;

import java.util.*;
import java.awt.*;
import java.time.format.DateTimeFormatter;

public class GUIMethods {
    // TaskManager taskList = new TaskManager();

    public Task saveInput(JComboBox<String> PriorityDD, JTextField UserTaskInput, JTextField UserDateInput){
        Hashtable <String,Integer> ht = new Hashtable<String,Integer>(); 
        ht.put("High",1);
        ht.put("Medium",2);
        ht.put("Low",3);

        String getUserTaskInput = UserTaskInput.getText();
        String getPriorityInput = PriorityDD.getSelectedItem().toString();
        String getDueDateInput = UserDateInput.getText();
        Task newTask = new Task(ht.get(getPriorityInput),getUserTaskInput,getDueDateInput);
    
        return newTask;

        
    }

    

}
