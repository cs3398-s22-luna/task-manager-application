package polishedgui.AddGUI;
import javax.swing.*;



public class Components {
    public JLabel setTitle(JLabel Title){
        Title.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        Title.setText("Create a new task by entering the data below:");
        return Title;
    }

    public JComboBox<String> getPriority(JComboBox<String> PriorityDD) {
        PriorityDD.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        PriorityDD.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Low", "Medium", "High" }));
        PriorityDD.setBorder(null);
        return PriorityDD;
    }

    public JTextField getDateInput(JTextField UserDateInput){ 
        UserDateInput.setText("MM-DD-YYYY");
        UserDateInput.setBorder(null);
        return UserDateInput;
    }
}
