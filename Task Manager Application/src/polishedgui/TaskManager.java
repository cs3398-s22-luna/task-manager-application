package polishedgui;

import java.lang.String;
import java.util.Vector;
import java.time.LocalDate;

// import taskmanager.Task;

public class TaskManager {
    protected Vector<Task> list;

    //constructor
    public TaskManager() {
        list = new Vector<Task>();
    }

    // adding task
    public void addTask(Task task){
        list.addElement(task);
    }
    
    public Vector<Task> getTaskList() {
        return list;
    }
    
    public int getTaskListSize() {
        return list.size();
    }
    
    public String getTask(int i) {
        return list.elementAt(i).getTitle();
    }
    
    public int getTaskIndex(String task) {
        for (int i = 0; i < list.size(); i++) {
            if (list.elementAt(i).getTitle().equals(task)) {
                return i;
            }
        }
        return 0;
    }
    
    public void removeTaskByTitle(String str) {
        for (int i = 0; i < list.size(); i++) {
            if(list.elementAt(i).getTitle().equals(str)) {
                list.removeElement(list.elementAt(i));
            }
        }
    }

    //removing task
    public void removeTask(Task t) {
        if (!list.isEmpty()) {
            for (Task task : list) {
                if(task.getTitle() == t.getTitle() && task.getEndDate() == t.getEndDate()) {
                    list.removeElement(task);
                }
            }
        }
    }

    // Setting task as complete.
    public void markAsDone(Task t) {
        for(int i = 0; i < list.size(); i++) {
            if(list.elementAt(i).getTitle() == t.getTitle()) {
                list.elementAt(i).setCompleted();
                break;
            }
        }
    }

    //Sort Tasks by Priority
    public void sortByPriority() {
        for (int i = 0; i <list.size(); i++) {
            Task next = list.elementAt(i);
            int j = i-1;
            while (j >= 0 && list.elementAt(i).getPriority() > next.getPriority()) {
                list.set(j + 1, list.elementAt(j));
                j = j-1;
                list.set(j +1, next);
            }
        }
    }

    public boolean changeTaskName(Task oldTask, Task newTask) {
        for (Task task : list) {
            if(task.getTitle() == oldTask.getTitle()) {
                list.set(list.indexOf(task), newTask);
                return true;
            }
        }
        return false;
    }

    public void sortByDate() {
        for (int i = 0; i <list.size(); i++) {
            Task next = list.elementAt(i);
            int j = i-1;
            while (j >= 0 && list.elementAt(i).getEndDate().compareTo(next.getEndDate()) < 0) {
                list.set(j + 1, list.elementAt(j));
                j = j-1;
                list.set(j +1, next);
            }
        }
    }
    
}//end of class
