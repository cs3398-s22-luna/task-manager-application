package test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Test;


public class dateFormat {
    final static String DATE_FORMAT = "MM-dd-yyyy";    // DATE_FORMAT = DATE_FORMAT.withResolverStyle(ResolverStyle.STRICT);

    public static boolean isDateValid(String date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        try {
            formatter.parse(date);
            return true;
        } catch (DateTimeParseException e) {
            return false;
        }
    }

     //                        HARD CODE TESTING 
     /// ----------------------- NOTHIGN HERE ------------------------- - //

     @Test //SANITY CHECK FOR DATE FORMAT.
     public void test1DateFormat(){
         Task test = new Task(1, "get high", "05-30-2025");
         String dateOutput = test.getDueDate();
         assertTrue("Valid date", isDateValid(dateOutput));
         // assertFalse("Valid date", !isDateValid(dateOutput));
     }
 
 
     @Test //SANITY CHECK FOR DATE FORMAT
     public void test2DateFormat(){
         Task test = new Task(1, "get high", "05/30/2025");
         String dateOutput = test.getDueDate();
         assertFalse("Invalid date, wrong format", isDateValid(dateOutput)); 
     }
 
     @Test //SANITY CHECK FOR DATE FORMAT
     public void test3DateFormat(){
         Task test = new Task(1, "get high", "05-50-2025");
         String dateOutput = test.getDueDate();
         assertFalse("Invalid date, days is invalid", isDateValid(dateOutput));
     }
 
     @Test //SANITY CHECK FOR DATE FORMAT
     public void test4DateFormat(){
         Task test = new Task(1, "get high", "69-02-2025");
         String dateOutput = test.getDueDate();
         assertFalse("Invalid date, months is invalid", isDateValid(dateOutput));
     }
 
     @Test //SANITY CHECK FOR DATE FORMAT
     public void test5DateFormat(){
         Task test = new Task(1, "get high", "69-69-2025");
         String dateOutput = test.getDueDate();
         assertFalse("Invalid date, days and months are invalid", isDateValid(dateOutput));
     }
 
     @Test //SANITY CHECK FOR DATE FORMAT
     public void test6DateFormat(){
         Task test = new Task(1, "get high", "01-01-1971");
         String dateOutput = test.getDueDate();
         assertTrue("Valid date", isDateValid(dateOutput));
     }
     
     @Test //SANITY CHECK FOR DATE FORMAT
     public void test7DateFormat(){
         Task test = new Task(1, "get high", "6969-696969-69696969");
         String dateOutput = test.getDueDate();
         assertFalse("COMPLETELY Valid date", isDateValid(dateOutput));
     }
 
     
}
