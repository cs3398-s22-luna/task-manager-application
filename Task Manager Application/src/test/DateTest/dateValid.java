package test;



public class dateValid {
    final static String DATE_FORMAT = "MM-dd-yyyy";

    public static boolean isDateValid(String s){
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        try{
            Date today = new Date();
            Date deadDate = dateFormat.parse(s);
            if(deadDate.compareTo(today) > 0){
                return true;
            }
            else{
                return false;
            }
        }
        catch(ParseException e){
            System.out.println("Date is bleh");
            return false;
        }
    }

    //HARD CODE TESTING
    @Test 
    public void test1DateValid(){
        Task test = new Task(1, "get high", "05-30-2025");
        // Task test = AddTaskGUI.getTaskForTesting();
        String dateOutput = test.getDueDate();
        assertTrue("Valid date", isDateValid(dateOutput));
    }

    @Test
    public void test2DateValid(){
        Task test = new Task(1, "get high", "05-30-2999");
        String dateOutput = test.getDueDate();
        assertTrue("Valid date", isDateValid(dateOutput));
    }

    @Test
    public void test3DateValid(){
        Task test = new Task(1, "get high", "05-30-1970");
        String dateOutput = test.getDueDate();
        assertFalse("DATE MUST BE GREATER THAN CURRENT DATE", isDateValid(dateOutput));
    }

}
