package polishedgui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import javax.management.DescriptorKey;

import org.junit.Test;


public class taskTest {    
    public Task test = new Task(1, "do homework", "05-30-2025");

    @Test
    @DescriptorKey("Test for get empty inputs")
    public void createNull(){
        Task t = new Task(-2, "", "");
        assertEquals(1, t.getPriority());
        assertEquals("", t.getTitle());
        assertEquals(LocalDate.now().toString(), t.getEndDate());
    }

    @Test
    @DescriptorKey("Test for get title")
    public void getTitleTest(){
        assertEquals("do homework", test.getTitle());
    }

    @Test
    @DescriptorKey("Test for get priority")
    public void getPriorityTest(){
        assertEquals(1, test.getPriority());
    }

    @Test
    @DescriptorKey("Test for get date")
    public void getDateTest(){
        assertEquals("05-30-2025", test.getEndDate());
    }

    @Test
    @DescriptorKey("Test for get completed")
    public void getCompletedTest(){
        assertFalse(test.getCompleted());
    }

    @Test
    @DescriptorKey("Test for set completed")
    public void setCompletedTest(){
        test.setCompleted();
        assertTrue(test.getCompleted());
    }

    @Test
    @DescriptorKey("Test for set title")
    public void setTitleTest(){
        test.setTitle("change title");
        assertEquals(test.getTitle(), "change title");
    }

    @Test
    @DescriptorKey("Test for set date")
    public void setDateTest(){
        test.setEndDate("02-17-2023");
        assertEquals("02-17-2023", test.getEndDate());        
    }

    @Test
    @DescriptorKey("Assert not equals date test")
    public void setDateFalseTest(){
        test.setEndDate("02-20-2023");
        assertNotEquals("02-17-2023", test.getEndDate());        
    }

    @Test
    @DescriptorKey("Assert not equals title test")
    public void setTitleFalseTest(){
        test.setTitle("title");
        assertNotEquals("02-17-2023", test.getTitle());        
    }

    @Test
    @DescriptorKey("Assert not equals priority test")
    public void setPriorityFalseTest(){
        test.setPriority(5);
        assertNotEquals(1, test.getPriority());        
    }

    @Test
    @DescriptorKey("Assert not equals completed test")
    public void setCompletedFalseTest(){
        test.setCompleted();
        assertNotEquals(false, test.getCompleted());        
    }
}
