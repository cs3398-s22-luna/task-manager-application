package polishedgui;

import java.awt.*;
import javax.swing.*;
import java.lang.String;
import java.time.LocalDate;


public class Task {

    //private variables
    private int priority;
    private String title;
    String dueDate;
    private boolean completed;

    //Constructor with tasks
    
    // constructor with all parameters
    public Task(int priorityParam, String titleParam, String date) {
        if (priorityParam < 0) {
            priorityParam = 1;
        }
        if (titleParam.compareTo(LocalDate.now().toString()) < 0) {
            date = LocalDate.now().toString();
        }
        priority = priorityParam;
        title = titleParam;
        dueDate = date;
        completed = false;
    }

    // setters for editing tasks
    protected void setPriority(int p){
        priority = p;
    }
    protected void setTitle(String t){
        title = t;
    }
   
    protected void setEndDate(String d){
       dueDate = d;
    }

    protected void setCompleted(){
        completed = true;
    }

    //getters to access task information
    public int getPriority(){
        return priority;
    }
    public String getTitle(){
        return title;
    }

    public String getEndDate(){
       return dueDate;
    }

    public boolean getCompleted(){
        return completed;
    }
    
}//end of class