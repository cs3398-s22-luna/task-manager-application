package taskmanager;

import java.awt.*;
import javax.swing.*;
import java.lang.String;
import java.time.LocalDate;


public class Task {

    //private variables
    private int priority;
    private String title;
    String dueDate;
    private boolean completed;
    
    //constructor with all parameters
    public Task(int priorityParam, String titleParam, String date) {
        priority = priorityParam;
        title = titleParam;
        dueDate = date;
        completed = false;
    }

    // setters for editing tasks
    protected void setPriority(int p){
        priority = p;
    }
    protected void setTitle(String t){
        title = t;
    }
    protected void setStartDate(String d){
        dueDate = d;
    }
<<<<<<< HEAD:Task Manager Application/taskmanager/Task.java

    protected void setEndDate(LocalDate d){
        endDate = d;
=======
//    protected void setEndDate(String d){
//        endDate = d;
//    }

    protected void setCompleted(){
        completed = true;
>>>>>>> master:OLD GUI CODE/taskmanager/Task.java
    }

    //getters to acces task information
    protected int getPriority(){
        return priority;
    }
    protected String getTitle(){
        return title;
    }
    protected String getStartDate(){
        return dueDate;
    }
//    protected LocalDate getEndDate(){
//        return endDate;
//    }

    protected boolean getCompleted(){
        return completed;
    }
    
}//end of class