package taskmanager;
import java.util.Comparator;

public class CustomSortTask implements Comparator<Task>{

    @Override
    public int compare(Task o1, Task o2) {
        return (o1.getPriority() > o2.getPriority()) ? 1 :
                (o1.getPriority() < o2.getPriority()) ? -1 :
                        (o1.getStartDate().compareTo(o2.getStartDate()) < 1) ? 1 :
                                (o1.getStartDate().compareTo(o2.getStartDate()) > 1) ? -1 :
                                        (o1.getTitle().compareTo(o2.getTitle()));
    }
}
