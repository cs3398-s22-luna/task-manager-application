package taskmanager;

import java.lang.String;
import javax.swing.table.DefaultTableModel;
import java.time.LocalDate;
import java.util.Vector;
import taskmanager.Task;

public class TaskManager {
    protected DefaultTableModel table;
    protected Vector<Task> list;

    //constructor
    public TaskManager() {
        table = new DefaultTableModel();
        list = new Vector<Task>();
    }

    //adding task

    public void addTask(int priority, String title, LocalDate begin, LocalDate end) {
        if (priority < 0) {
            priority = 0;
        }
        if (begin.isBefore(LocalDate.now())) {
            begin = LocalDate.now();
        }
        if (end.isBefore(begin)) {
            end = begin.plusWeeks(1);
        }
        list.add(new Task (priority, title, begin, end));
    }

    public void addTask( Task k){
        if (k.getPriority() < 0) {
            k.setPriority(0);
        }
        if (k.getStartDate().isBefore(LocalDate.now())) {
            k.setStartDate(LocalDate.now());
        }
        if (k.getEndDate().isBefore(begin)) {
            k.setEndDate(LocalDate.now().plusWeeks(1));
        }
    }

    //removing task
    public void removeTask(Task t) {
        for(int i = 0; i < list.size(); i++) {
            if(list.elementAt(i).getTitle() == t.getTitle()){
                list.remove(i);
                break;
            }
        }
    }

    // Setting task as complete.
    public void markAsDone(Task t) {
        for(int i = 0; i < list.size(); i++) {
            if(list.elementAt(i).getTitle() == t.getTitle()) {
                list.elementAt(i).setCompleted();
                break;
            }
        }
    }

    //Sort Tasks by Priority
    public void sortByPriority() {
        for (int i = 0; i <list.size(); i++) {
            Task next = list.elementAt(i);
            int j = i-1;
            while (j >= 0 && list.elementAt(i).getPriority() > next.getPriority()) {
                list.set(j + 1, list.elementAt(j));
                j = j-1;
                list.set(j +1, next);
            }
        }
    }

    public DefaultTableModel getAsTable() {
        while (table.getRowCount() > 0) {
            table.removeRow(0);
        }
        table.addRow(list);
        return table;
    }

    // Edit Task Name, returns true if updated
    public boolean changeTaskName(Task oldTask, Task newTask) {
        for (Task task : list) {
            if(task.getTitle() == oldTask.getTitle()) {
                list.set(list.indexOf(task), newTask);
                return true;
            }
        }
        return false;
    }

    public void sortByDate() {
        for (int i = 0; i <list.size(); i++) {
            Task next = list.elementAt(i);
            int j = i-1;
            while (j >= 0 && list.elementAt(i).getStartDate().isAfter(next.getStartDate())) {
                list.set(j + 1, list.elementAt(j));
                j = j-1;
                list.set(j +1, next);
            }
        }
    }
    
}//end of class
